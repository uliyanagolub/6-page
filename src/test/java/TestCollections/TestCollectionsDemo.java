package TestCollections;

import AboutHuman.Human;
import AboutHuman.ListDemo;
import AboutHuman.Student;
import org.testng.Assert;
import org.testng.annotations.Test;
import Collections.*;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class TestCollectionsDemo {
    @Test
    public void testAmountOfStringsStartWithSymbol(){
        List<String> list = new ArrayList<>();
        list.add("Hello");
        list.add("July");
        list.add("Hero");
        assertEquals(2, CollectionsDemo.sameFirstSymbol(list, 'H'));
        assertEquals(0, CollectionsDemo.sameFirstSymbol(list, 'W'));
        assertEquals(0, CollectionsDemo.sameFirstSymbol(new ArrayList<>(), 'W'));
    }

    @Test
    public void testGetListWithSameSecondName(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("Willson", "Andy", "", 13));
        list.add(new Human("Willson", "Martin", "", 15));
        list.add(new Human("Stauber", "Jack", "", 19));

        List<Human> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(0));
        rightAnswer.add(list.get(1));

        assertEquals(rightAnswer, ListDemo.sameSurnamePeople(list, new Human("Willson", "", "", 10)));
        assertEquals(new ArrayList<>(), ListDemo.sameSurnamePeople(new ArrayList<>(), new Human("Willson", "", "", 10)));
    }

    @Test
    public void testGetHumanListByHumanSecondName(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("Andy", "Willson", "", 13));
        list.add(new Human("Martin", "Willson", "", 15));
        list.add(new Human("Jack", "Stauber", "", 19));

        List<Human> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(0));
        rightAnswer.add(list.get(1));

        Assert.assertEquals(rightAnswer, ListDemo.deleteHuman(list, new Human("Jack", "Stauber", "", 19)));
    }

    @Test
    public void testGetNotIntersectedSets(){
        List<Set<Integer>> list = new ArrayList<>();
        list.add(new HashSet<>());
        list.add(new HashSet<>());
        list.add(new HashSet<>());
        list.get(1).add(1);
        list.get(2).add(2);
        list.get(2).add(3);

//        Collections.addAll(list.get(1), 1,2,3,4)
        HashSet<Integer> set = new HashSet<>();
        set.add(1);

        List<Set<Integer>> rightAnswer = new ArrayList<>();
        rightAnswer.add(list.get(0));
        rightAnswer.add(list.get(2));

        assertEquals(rightAnswer, ListDemo.noIntersectionSet(list, set));
    }

    @Test
    public void testGetHumansWithMaxAge(){
        List<Human> list = new ArrayList<>();
        list.add(new Human("","", "", 15));
        list.add(new Student("Fiona", "", "", 19, ""));
        list.add(new Student("Kelly", "", "", 19, ""));
        list.add(new Human("", "", "", 17));

        Set<Human> rightAnswer = new HashSet<>();
        rightAnswer.add(list.get(1));
        rightAnswer.add(list.get(2));

        assertEquals(rightAnswer, ListDemo.humanMaxAge(list));
    }

    @Test
    public void testHumansByIdentity(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 14));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 16));
        map.put(4, new Human("", "", "", 17));

        Set<Integer> set = new HashSet<>();
        set.add(2);
        set.add(3);

        Set<Human> rightAnswer = new HashSet<>();
        rightAnswer.add(map.get(2));
        rightAnswer.add(map.get(3));

        assertEquals(rightAnswer, ListDemo.setHumanId(map, set));
    }

    @Test
    public void testHumanIdAboveEighteen(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 25));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 37));
        map.put(4, new Human("", "", "", 17));

        List<Integer> rightAnswer = new ArrayList<>();
        rightAnswer.add(1);
        rightAnswer.add(3);

        Assert.assertEquals(rightAnswer, ListDemo.setHumanAge(map));
    }

    @Test
    public void testGetAgeByIdentity(){
        Map<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("", "", "", 25));
        map.put(2, new Human("", "", "", 15));
        map.put(3, new Human("", "", "", 37));
        map.put(4, new Human("", "", "", 17));

        Map<Integer, Integer> rightAnswer = new HashMap<>();
        rightAnswer.put(1, 25);
        rightAnswer.put(2, 15);
        rightAnswer.put(3, 37);
        rightAnswer.put(4, 17);

        Assert.assertEquals(rightAnswer, ListDemo.setHumanIdAge(map));
    }

    @Test
    public void testGetAgeHumanMap(){
        Set<Human> set = new HashSet<>();
        set.add(new Human("Andy", "", "", 17));
        set.add(new Human("Mary", "", "", 17));
        set.add(new Human("Mark", "", "", 23));
        set.add(new Human("Veronica", "", "", 21));

        Map<Integer, List<Human>> rightAnswer = new HashMap<>();
        rightAnswer.put(17, new ArrayList<>());
        rightAnswer.put(23, new ArrayList<>());
        rightAnswer.put(21, new ArrayList<>());
        rightAnswer.get(17).add(new Human("Andy", "", "", 17));
        rightAnswer.get(17).add(new Human("Mary", "", "", 17));
        rightAnswer.get(23).add(new Human("Mark", "", "", 23));
        rightAnswer.get(21).add(new Human("Veronica", "", "", 21));

        Assert.assertEquals(rightAnswer, ListDemo.mapOfHumanAge(set));
    }
}