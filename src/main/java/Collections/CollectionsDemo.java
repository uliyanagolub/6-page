package Collections;

import java.util.List;

/*Вход: список строк и символ. Выход: количество строк входного списка, у которых первый
символ совпадает с заданным.*/

public class CollectionsDemo {
    public static int sameFirstSymbol(List<String> list, char symbol){
        int sum = 0;
        for (String element: list){
            if (element == null){
                continue;
            }
            if (element.charAt(0) == symbol){
                sum++;
            }
        }
        return sum;
    }
}
