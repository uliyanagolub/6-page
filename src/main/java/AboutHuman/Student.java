package AboutHuman;

//класс Student, производный от Human
public class Student extends Human{

    //новое поле — название факультета
    private String faculty;
    
    //геттер
    public String getFaculty() {
        return faculty;
    }
    
    //сеттер
    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    //конструктор
    public Student(String surname, String name, String patronymic, int sge, String faculty) {
        super(surname, name, patronymic, sge);
        this.faculty = faculty;
    }

}
