package AboutHuman;

import java.util.*;

public class ListDemo {

    /*Метод, который получает на вход список объектов типа Human
    и еще один объект типа Human. Результат — список однофамильцев заданного человека
    среди людей из входного списка.*/

    public static List<Human> sameSurnamePeople(List<Human> list1, Human human){
        List<Human> humanList = new ArrayList<>();
        for (Human element: list1){
            if (element.getSurname().equals(human.getSurname())){
                humanList.add(element);
            }
        }
        return humanList;
    }

    /*Вход: список объектов типа Human и еще один объект типа Human. Выход — копия
    входного списка, не содержащая выделенного человека. При изменении элементов
    входного списка элементы выходного изменяться не должны.*/
    
    public static List<Human> deleteHuman(List<Human> humanList, Human human){
        ArrayList<Human> resultList = new ArrayList<>();
        for (Human element: humanList){
            if (!element.equals(human)){
                resultList.add(element);
            }
        }
        return resultList;
    }

    /*Вход: список множеств целых чисел и еще одно множество. Выход: список всех множеств
    входного списка, которые не пересекаются с заданным множеством. */
    
    public static List<Set<Integer>> noIntersectionSet(List<Set<Integer>> list, Set<Integer> integerSet){
        boolean flag = true;
        ArrayList<Set<Integer>> result = new ArrayList<>();
        for (Set<Integer> element: list){
            flag = true;
            for (Integer i: element){
                if (integerSet.contains(i)) {
                    flag = false;
                    break;
                }
            }
            if(flag){
                result.add(element);
            }
        }
        return result;
    }

    /* Vетод, который получает на вход список, состоящий из
    объектов типа Human и его производных классов. Результат — множество людей из
    входного списка с максимальным возрастом */

    public static Set<Human> humanMaxAge(List<? extends Human> humans){
        Set<Human> result = new HashSet<>();
        int maxAge = 0;
        for(Human element: humans){
            if (element.getAge()> maxAge){
                maxAge = element.getAge();
            }
        }
        for (Human element: humans){
            if (element.getAge() == maxAge){
                result.add(element);
            }
        }
        return result;
    }

    /* Метод, который получает на вход отображение (Map)
    целочисленных идентификаторов в объекты типа Human и множество целых чисел.
    Результат — множество людей, идентификаторы которых содержатся во входном множестве.*/
    
    public static Set<Human> setHumanId(Map<Integer, Human> humanMap, Set<Integer> integerSet){
        Set<Human> result = new HashSet<>();
        for(Map.Entry<Integer, Human> element: humanMap.entrySet()){
            if(integerSet.contains(element.getKey())){
                result.add(element.getValue());
            }
        }
        return result;
    }

    // Список идентификаторов людей, чей возраст не менее 18 лет.
    
    public static List<Integer> setHumanAge(Map <Integer, Human> humanMap){
        List <Integer> result = new ArrayList<>();
        for (Map.Entry<Integer, Human> element: humanMap.entrySet()){
            if (element.getValue().getAge() >= 18){
                result.add(element.getKey());
            }
        }
        return result;
    }

    // Отображение, которое идентификатору сопоставляет возраст человека.

    public static Map <Integer, Integer> setHumanIdAge(Map<Integer, Human> humanMap){
        Map<Integer, Integer> result = new HashMap<>();
        for (Map.Entry<Integer, Human> element: humanMap.entrySet()){
            result.put(element.getKey(),element.getValue().getAge());
        }
        return result;
    }

    /*По множеству объектов типа Human постройте отображение, которое целому числу (возраст человека) 
    сопоставляет список всех людей данного возраста из входного множества.*/

    public static Map<Integer, List<Human>> mapOfHumanAge(Set<Human> humans){
        Map<Integer, List<Human>> result = new HashMap<>();
        for (Human element: humans){
            if (!result.containsKey(element.getAge())){
                result.put(element.getAge(), new ArrayList<>());
            }
            result.get(element.getAge()).add(element);
        }
        return result;
    }


}
